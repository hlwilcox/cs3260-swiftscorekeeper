//
//  ViewController.swift
//  SwiftScoreKeeper
//
//  Created by Heather Wilcox on 2/16/18.
//  Copyright © 2018 Heather Wilcox. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    var team1ScoreLbl = UILabel()
    var team2ScoreLbl = UILabel()
    var team1Stepper = UIStepper()
    var team2Stepper = UIStepper()
    var resetBtn = UIButton(type: .system)

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        let holdingViewLeft = UIView()
        holdingViewLeft.translatesAutoresizingMaskIntoConstraints = false
        holdingViewLeft.backgroundColor = .white
        self.view.addSubview(holdingViewLeft)
        
        let holdingViewRight = UIView()
        holdingViewRight.translatesAutoresizingMaskIntoConstraints = false
        holdingViewRight.backgroundColor = .white
        self.view.addSubview(holdingViewRight)
        
        let team1TxtFld = UITextField()
        team1TxtFld.translatesAutoresizingMaskIntoConstraints = false
        team1TxtFld.backgroundColor = .yellow
        team1TxtFld.text = "Team 1 TextField"
        holdingViewLeft.addSubview(team1TxtFld)
        
        let team2TxtFld = UITextField()
        team2TxtFld.translatesAutoresizingMaskIntoConstraints = false
        team2TxtFld.backgroundColor = .yellow
        team2TxtFld.text = "Team 2 TextField"
        holdingViewRight.addSubview(team2TxtFld)
        
        //team1ScoreLbl = UILabel()
        team1ScoreLbl.translatesAutoresizingMaskIntoConstraints = false
        team1ScoreLbl.backgroundColor = .green
        team1ScoreLbl.text = "0"
        holdingViewLeft.addSubview(team1ScoreLbl)
        
        //team2ScoreLbl = UILabel()
        team2ScoreLbl.translatesAutoresizingMaskIntoConstraints = false
        team2ScoreLbl.backgroundColor = .green
        team2ScoreLbl.text = "0"
        holdingViewRight.addSubview(team2ScoreLbl)
        
        //team1Stepper = UIStepper()
        team1Stepper.translatesAutoresizingMaskIntoConstraints = false
        team1Stepper.addTarget(self, action: #selector(team1StepperValueChanged(_:)), for: .touchDown)
        team1Stepper.maximumValue = 21
        team1Stepper.value = 1
        holdingViewLeft.addSubview(team1Stepper)
        
        //team2Stepper = UIStepper()
        team2Stepper.translatesAutoresizingMaskIntoConstraints = false
        team2Stepper.addTarget(self, action: #selector(team2StepperValueChanged(_:)), for: .touchDown)
        team2Stepper.maximumValue = 21
        team2Stepper.value = 1
        holdingViewRight.addSubview(team2Stepper)
        
        //resetBtn = UIButton(type: .system)
        resetBtn.translatesAutoresizingMaskIntoConstraints = false
        resetBtn.addTarget(self, action: #selector(resetBtn(_:)), for: .touchDown)
        resetBtn.backgroundColor = .blue
        resetBtn.setTitle("Reset", for: .normal)
        self.view.addSubview(resetBtn)
        
        //key value pairing
        let views: [String: Any] = [
            "team1TxtFld":team1TxtFld,
            "team2TxtFld":team2TxtFld,
            "team1ScoreLbl":team1ScoreLbl,
            "team2ScoreLbl":team2ScoreLbl,
            "team1Stepper":team1Stepper,
            "team2Stepper":team2Stepper,
            "resetBtn":resetBtn,
            "holdingViewLeft":holdingViewLeft,
            "holdingViewRight":holdingViewRight]
        
        var allConstraints: [NSLayoutConstraint] = []
        
        let holdingViewHorizontalConstraints = NSLayoutConstraint.constraints(withVisualFormat: "H:|[holdingViewLeft(==holdingViewRight)][holdingViewRight(==holdingViewLeft)]|", metrics: nil, views: views)
        allConstraints += holdingViewHorizontalConstraints
        
        let holdingViewLeftVerticalConstraints = NSLayoutConstraint.constraints(withVisualFormat: "V:|[holdingViewLeft]|", metrics: nil, views: views)
        allConstraints += holdingViewLeftVerticalConstraints
        
        let holdingViewRightVerticalConstraints = NSLayoutConstraint.constraints(withVisualFormat: "V:|[holdingViewRight]|", metrics: nil, views: views)
        allConstraints += holdingViewRightVerticalConstraints
        
        let team1TxtFldConstraints = NSLayoutConstraint.constraints(withVisualFormat: "H:|-10-[team1TxtFld]-10-|", metrics: nil, views: views)
        allConstraints += team1TxtFldConstraints
        
        let team2TxtFldConstraints = NSLayoutConstraint.constraints(withVisualFormat: "H:|-10-[team2TxtFld]-10-|", metrics: nil, views: views)
        allConstraints += team2TxtFldConstraints
        
        let team1ScoreLblConstraints = NSLayoutConstraint.constraints(withVisualFormat: "H:|-10-[team1ScoreLbl]-10-|", metrics: nil, views: views)
        allConstraints += team1ScoreLblConstraints
        
        let team2ScoreLblConstraints = NSLayoutConstraint.constraints(withVisualFormat: "H:|-10-[team2ScoreLbl]-10-|", metrics: nil, views: views)
        allConstraints += team2ScoreLblConstraints
        
        let team1StepperConstraints = NSLayoutConstraint.constraints(withVisualFormat: "H:|-10-[team1Stepper]-10-|", metrics: nil, views: views)
        allConstraints += team1StepperConstraints
        
        let team2StepperConstraints = NSLayoutConstraint.constraints(withVisualFormat: "H:|-10-[team2Stepper]-10-|", metrics: nil, views: views)
        allConstraints += team2StepperConstraints
        
        let resetBtnConstraints = NSLayoutConstraint.constraints(withVisualFormat: "H:|[resetBtn]|", metrics: nil, views: views)
        allConstraints += resetBtnConstraints
        
        let leftVerticalConstraints = NSLayoutConstraint.constraints(withVisualFormat: "V:|-60-[team1TxtFld]-[team1ScoreLbl(>=120)]-[team1Stepper]-[resetBtn(55)]|", metrics: nil, views: views)
        allConstraints += leftVerticalConstraints
        
        let rightVerticalConstraints = NSLayoutConstraint.constraints(withVisualFormat: "V:|-60-[team2TxtFld]-[team2ScoreLbl(>=120)]-[team2Stepper]-[resetBtn(55)]|", metrics: nil, views: views)
        allConstraints += rightVerticalConstraints
        
        NSLayoutConstraint.activate(allConstraints)
    }

    @objc func team1StepperValueChanged(_ sender : UIStepper) {
        team1ScoreLbl.text = String(format:"%.f", sender.value)
    }
    
    @objc func team2StepperValueChanged(_ sender : UIStepper) {
        team2ScoreLbl.text = String(format:"%.f", sender.value)
    }
    
    @objc func resetBtn(_ sender : UIButton) {
        team1ScoreLbl.text = "0"
        team2ScoreLbl.text = "0"
        team1Stepper.value = 0
        team2Stepper.value = 0
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}
